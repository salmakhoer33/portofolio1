import "./App.css";
import NavigationBar from "./components/NavigationBar";
import Intro from "./components/Intro";
import Wanita from "./components/Wanita";
import Pria from "./components/Pria";
import Footer from "./components/Footer";
import "./style/landingPage.css";

function App() {
  return (
    <div>
      <div className="myBG border">
        <NavigationBar />
        <Intro />
      </div>
      <div className="wanita">
        <Wanita />
      </div>
      <div className="pria">
        <Pria />
      </div>
      <div className="footer">
        <Footer />
      </div>
    </div>
  );
}

export default App;
