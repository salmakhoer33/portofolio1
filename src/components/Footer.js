import React from "react";

function Footer() {
  return (
    <div className="footer-container">
      <div className="footer">
        <div className="footer-text">
          <p>
            Seluruh isi dan informasi dari situs ini dilindungi hak cipta dan
            merupakan FASHION FRENZY. Layanan Pengaduan Konsumen, Direktorat
            Jenderal Perlindungan Konsumen dan Tertib Niaga, Kementerian
            Perdagangan Republik Indonesia, 0853-1111-112 (WhatsApp)
          </p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
