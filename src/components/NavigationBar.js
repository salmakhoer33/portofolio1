import { Navbar, Container, Nav } from "react-bootstrap";

const NavigationBar = () => {
  return (
    <div>
      <Navbar className="bg-light variant-dark expand-lg fixed-top">
        <Container>
          <Navbar.Brand>FASHION FRENZY</Navbar.Brand>
          <Nav>
            <Nav.Link>WANITA</Nav.Link>
            <Nav.Link>PRIA</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavigationBar;
