import { Card, Container, Row, Col, Image } from "react-bootstrap";
import men1 from "../assets/h&m media/men1.jpg";
import men2 from "../assets/h&m media/men2.jpg";
import men3 from "../assets/h&m media/men3.jpg";

const Pria = () => {
  return (
    <div>
      <div className="bannerPria">
        <Container className="text-white d-flex justify-content-center text-center align-items-center">
          <Row>
            <Col>
              <p className="fst-italic">Temukan Shirt pilihan Anda di sini</p>
            </Col>
          </Row>
        </Container>
      </div>
      <Container>
        <Row>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={men1} alt="men1" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">
                      Regular Fit - Shirt
                    </Card.Title>
                    <Card.Text className="text-left">Rp899,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={men2} alt="men2" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">
                      Regular Fit Overshirt
                    </Card.Title>
                    <Card.Text className="text-left">Rp729,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={men3} alt="men3" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">
                      Relaxed Fit Sweatshirt
                    </Card.Title>
                    <Card.Text className="text-left">Rp459,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Pria;
