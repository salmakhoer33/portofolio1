import { Col, Container, Row, Button, Image, Carousel } from "react-bootstrap";
import newCover1 from "../assets/h&m media/newCover1.jpg";
import newCover2 from "../assets/h&m media/newCover2.jpg";
import newCover3 from "../assets/h&m media/newCover3.jpg";

const Intro = () => {
  return (
    <div>
      <div className="intro">
        <Container className="text-white d-flex justify-content-center text-center align-items-center">
          <Row>
            <Col>
              <div className="title">FASHION FRENZY</div>
              <div className="title">OUTFIT MASA KINI</div>
              <div className="introButton mt-4 text-center">
                <Button variant="dark">Belanja Sekarang</Button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="carouselPart">
        <Carousel>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={newCover1}
              alt="First slide"
            />
            <Carousel.Caption>
              <h2>Tampil stylish dengan warna yang cerah</h2>
              <p>Koleksi yang fresh untuk Anda sekarang !</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={newCover2}
              alt="Second slide"
            />

            <Carousel.Caption>
              <h2>New: Casual Outfits 2023</h2>
              <p>Dapatkan sekarang !</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={newCover3}
              alt="Third slide"
            />

            <Carousel.Caption>
              <h2>Hoodie Colection</h2>
              <p>Dapatkan Disc 20% untuk Hoodie Colection </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  );
};

export default Intro;
