import { Card, Container, Row, Col, Image } from "react-bootstrap";
import girl1 from "../assets/h&m media/girl1.jpg";
import girl2 from "../assets/h&m media/girl2.jpg";
import girl3 from "../assets/h&m media/girl3.jpg";
import iconLove from "../assets/h&m media/iconLove.png";

const Wanita = () => {
  return (
    <div>
      <div className="bannerWanita">
        <Container className="text-white d-flex justify-content-center text-center align-items-center">
          <Row>
            <Col>
              <p className="fst-italic">Easy to Style, to wear, and to love</p>
            </Col>
          </Row>
        </Container>
      </div>
      <Container>
        <Row>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={girl1} alt="girl1" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">
                      Oversized Shirt
                    </Card.Title>
                    <Card.Text className="text-left">Rp499,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={girl2} alt="girl1" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">White Pants</Card.Title>
                    <Card.Text className="text-left">Rp329,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
          <Col md={4} className="girlsIamges">
            <Col className="girlsImg">
              <Card className="  girlsImg">
                <Image src={girl3} alt="girl1" />
                <div className="bg-white">
                  <div className="p-2 m-1 text-black">
                    <Card.Title className="text-center">
                      {" "}
                      Fitted Vest{" "}
                    </Card.Title>
                    <Card.Text className="text-left">Rp549,900</Card.Text>
                    <Card.Text className="text-left">PRODUK TERBARU</Card.Text>
                  </div>
                </div>
              </Card>
            </Col>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Wanita;
